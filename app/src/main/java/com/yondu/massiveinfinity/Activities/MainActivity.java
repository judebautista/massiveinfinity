package com.yondu.massiveinfinity.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.yondu.massiveinfinity.Adapters.DeliveryAdapter;
import com.yondu.massiveinfinity.Interfaces.DeliveryAdapterCallback;
import com.yondu.massiveinfinity.Interfaces.MainPresenterCallback;
import com.yondu.massiveinfinity.Mangers.DialogManager;
import com.yondu.massiveinfinity.Models.Delivery;
import com.yondu.massiveinfinity.Presenters.MainPresenter;
import com.yondu.massiveinfinity.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, DeliveryAdapterCallback, MainPresenterCallback {

    LinearLayoutManager mLayoutManager;
    DeliveryAdapter deliveryAdapter;
    MainPresenter mainPresenter;
    RecyclerView delivery_rv;
    SwipeRefreshLayout swipeRefreshLayout;
    List<Delivery.Response> deliveryList;
    TextView title;
    DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    public void init() {
        delivery_rv  = (RecyclerView)findViewById(R.id.delivery_rv);
        title = (TextView)findViewById(R.id.dashboard_title);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        deliveryList = new ArrayList<>();
        mainPresenter = new MainPresenter(this);
        mLayoutManager = new LinearLayoutManager(this);
        dialogManager = new DialogManager(this);
        deliveryAdapter = new DeliveryAdapter(this,deliveryList);
        deliveryAdapter.setCallback(this);
        mainPresenter.setCallback(this);
        delivery_rv.setItemAnimator(new DefaultItemAnimator());
        delivery_rv.setLayoutManager(mLayoutManager);
        delivery_rv.setAdapter(deliveryAdapter);
        swipeRefreshLayout.setOnRefreshListener(this);

        title.setText("Thinks to Deliver");
    }

    public void loadDeliveryData(List<Delivery.Response> deliveryListResponse )
    {
        if(deliveryListResponse.size() != 0) {
            deliveryList.clear();
            deliveryList.addAll(deliveryListResponse);
            delivery_rv.getRecycledViewPool().clear();
            deliveryAdapter.notifyDataSetChanged();
        }
        else
        {
            Toast.makeText(this,"No Data Found",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.getDeliveryDetails();
    }

    // callback for swipe to refresh
    @Override
    public void onRefresh() {
        delivery_rv.setLayoutFrozen(true);
        mainPresenter.getDeliveryDetails();
    }
    // callback for MainPresenter
    @Override
    public void onSuccessDeliveryCallback(List<Delivery.Response> deliveryList) {
        swipeRefreshLayout.setRefreshing(false);
        delivery_rv.setLayoutFrozen(false);
        loadDeliveryData(deliveryList);
    }
    @Override
    public void onFailedDeliveryCallback() {
        swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(this,"Internet Problem Connection",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void noConnection() {
        dialogManager.showDialog("Connection Problem","No Internet Connection");
    }
    // Callack for Click Listener
    @Override
    public void onClick(Delivery.Response deliveryInfo) {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("deliveryInfo", deliveryInfo);
        startActivity(intent);
    }
}
