package com.yondu.massiveinfinity.Activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.yondu.massiveinfinity.Models.Delivery;
import com.yondu.massiveinfinity.R;
import com.yondu.massiveinfinity.utils.MapHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    LatLng position;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    MapHelper mapHelper;
    Delivery.Response deliveryInfo;
    TextView deliveryDetails;
    ImageView deliveryLogo;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        deliveryInfo = (Delivery.Response) getIntent().getSerializableExtra("deliveryInfo");
        position = new LatLng(deliveryInfo.getLocation().getLat(), deliveryInfo.getLocation().getLng());
        init();
    }

    public void init() {
        title = (TextView) findViewById(R.id.dashboard_title);
        deliveryDetails = (TextView) findViewById(R.id.map_title);
        deliveryLogo = (ImageView) findViewById(R.id.map_logo);
        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).build();

        if (!deliveryInfo.getImageUrl().toString().equals(""))
            Picasso.with(this).load(deliveryInfo.getImageUrl().toString()).networkPolicy(NetworkPolicy.OFFLINE).into(deliveryLogo);
        deliveryDetails.setText(deliveryInfo.getDescription() + " at " + deliveryInfo.getLocation().getAddress());

        title.setText("Deliver Details");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mapHelper.getMapWidget(this, mGoogleMap);
        mapHelper.addMarkerGoogleMap(mGoogleMap, position, deliveryInfo.getLocation().getAddress(), BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mapHelper.UpdateGoogleCamera(mGoogleMap, position, 15, 0);

    }


}
