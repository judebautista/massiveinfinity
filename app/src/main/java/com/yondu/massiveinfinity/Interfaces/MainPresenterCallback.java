package com.yondu.massiveinfinity.Interfaces;

import com.yondu.massiveinfinity.Models.Delivery;

import java.util.List;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public interface MainPresenterCallback {
    void onSuccessDeliveryCallback(List<Delivery.Response> response);
    void onFailedDeliveryCallback();
    void noConnection();
}
