package com.yondu.massiveinfinity.Interfaces;


import com.yondu.massiveinfinity.Models.Delivery;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Jude Bautista on 05/06/2017.
 */

public interface APIInterface {


    @GET("/deliveries")
    Observable<List<Delivery.Response>> deliveries();


}
