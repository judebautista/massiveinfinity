package com.yondu.massiveinfinity.Interfaces;

import com.yondu.massiveinfinity.Models.Delivery;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public interface DeliveryAdapterCallback {

    void onClick(Delivery.Response deliveryInfo);
}
