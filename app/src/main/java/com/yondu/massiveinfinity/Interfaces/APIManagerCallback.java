package com.yondu.massiveinfinity.Interfaces;


/**
 * Created by Jude Bautista on 05/06/2017.
 */

public interface APIManagerCallback {
    void onSuccess(Object response);
    void onFailure(String hiidenStatus);
}
