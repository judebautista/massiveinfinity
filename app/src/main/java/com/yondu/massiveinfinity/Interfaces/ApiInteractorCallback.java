package com.yondu.massiveinfinity.Interfaces;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public interface ApiInteractorCallback {
    void onSuccesss(Object response);
    void onFailure();

}
