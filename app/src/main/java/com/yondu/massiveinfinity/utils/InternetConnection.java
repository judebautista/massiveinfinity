package com.yondu.massiveinfinity.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Jude Bautista on 05/05/2017.
 */

public class InternetConnection {

    Context mContext;
    boolean connected = false;

    public InternetConnection(Context context) {
        mContext = context;
    }


    public Boolean isWifiAndDataEnabled() {

        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return connected = true;
        } else
            return connected = false;
    }
}
