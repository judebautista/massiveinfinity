package com.yondu.massiveinfinity.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Jude Bautista on 22/08/2017.
 */

public class MapHelper {

    // Map widget
    public static void getMapWidget(Context context,GoogleMap mGoogleMap) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(true);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.setMinZoomPreference(10.0f);
        mGoogleMap.setMaxZoomPreference(20.0f);
    }
    // Add marker
    public static void addMarkerGoogleMap(GoogleMap googleMap, LatLng position, String title, BitmapDescriptor design)
    {
        MarkerOptions optionOrigin = new MarkerOptions();
        optionOrigin.icon(design);
        optionOrigin.position(position);
        optionOrigin.title(title);
        googleMap.addMarker(optionOrigin);
    }
    // Update Position
    public static void UpdateGoogleCamera(GoogleMap mGoogleMap,LatLng position,float zoomLevel,float degreee)
    {
        CameraPosition cameraPosition = CameraPosition.builder().target(position).zoom(zoomLevel).bearing(degreee).tilt(30).build();
        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }
}
