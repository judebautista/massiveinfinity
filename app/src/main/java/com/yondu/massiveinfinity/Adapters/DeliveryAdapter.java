package com.yondu.massiveinfinity.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yondu.massiveinfinity.Interfaces.DeliveryAdapterCallback;
import com.yondu.massiveinfinity.Models.Delivery;
import com.yondu.massiveinfinity.R;

import java.util.List;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.ViewHolder>  {

    public List<Delivery.Response> deliveryList;
    public View view;
    DeliveryAdapterCallback mCallback;
    Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder  {
        ImageView logo;
        TextView title;
        LinearLayout deliveryItem;

        public ViewHolder(View v) {
            super(v);
            logo = (ImageView)v.findViewById(R.id.delivery_item_logo);
            title = (TextView) v.findViewById(R.id.delivery_item_title);
            deliveryItem = (LinearLayout)v.findViewById(R.id.delivery_item);
        }


    }

    public DeliveryAdapter(Context context,List<Delivery.Response> deliveryList) {
        mContext =context;
        this.deliveryList = deliveryList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_items, parent, false);
        return new DeliveryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Delivery.Response item = deliveryList.get(position);
        holder.setIsRecyclable(false);

        if(!item.getImageUrl().toString().equals(""))
        Picasso.with(mContext).load(item.getImageUrl().toString()).into(holder.logo);

        holder.title.setText(item.getDescription()+" at "+item.getLocation().getAddress());
        holder.deliveryItem.setTag(item);

        holder.deliveryItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onClick((Delivery.Response) holder.deliveryItem.getTag());
            }
        });
    }

    public void setCallback(DeliveryAdapterCallback callback)
    {
        mCallback = callback;
    }


    @Override
    public int getItemCount() {
        return deliveryList.size();
    }


}




