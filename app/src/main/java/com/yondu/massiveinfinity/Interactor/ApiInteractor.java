package com.yondu.massiveinfinity.Interactor;

import com.yondu.massiveinfinity.Interfaces.APIManagerCallback;
import com.yondu.massiveinfinity.Interfaces.ApiInteractorCallback;
import com.yondu.massiveinfinity.Mangers.APIManager;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public class ApiInteractor implements APIManagerCallback {

    APIManager apiManager;
    ApiInteractorCallback mCallback;
    public ApiInteractor() {
        apiManager = new APIManager();
        apiManager.setCallback(this);
    }

    public void getDeliveryDetails()
    {
        apiManager.getDeliveries();
    }
    // callback for ApiManager
    @Override
    public void onSuccess(Object response) {
        mCallback.onSuccesss(response);
    }

    @Override
    public void onFailure(String hiidenStatus) {
        mCallback.onFailure();
    }

    public void setCallback(ApiInteractorCallback callback)
    {
        mCallback = callback;
    }
}
