package com.yondu.massiveinfinity.Presenters;

import android.content.Context;

import com.yondu.massiveinfinity.Interactor.ApiInteractor;
import com.yondu.massiveinfinity.Interfaces.ApiInteractorCallback;
import com.yondu.massiveinfinity.Interfaces.MainPresenterCallback;
import com.yondu.massiveinfinity.Mangers.DialogManager;
import com.yondu.massiveinfinity.Models.Delivery;
import com.yondu.massiveinfinity.utils.InternetConnection;

import java.util.List;

/**
 * Created by Jude Bautista on 20/08/2017.
 */

public class MainPresenter implements ApiInteractorCallback {
    MainPresenterCallback mCallback;
    ApiInteractor apiInteractor;
    InternetConnection internetConnection;
    Context mContext;

    public MainPresenter(Context context) {
        mContext = context;
        apiInteractor = new ApiInteractor();
        apiInteractor.setCallback(this);
        internetConnection = new InternetConnection(mContext);
    }

    public void getDeliveryDetails() {
        if (internetConnection.isWifiAndDataEnabled())
            apiInteractor.getDeliveryDetails();
        else
            mCallback.noConnection();
    }
    // callback for Api Interactor
    @Override
    public void onSuccesss(Object response) {
        List<Delivery.Response> deliveryList = (List<Delivery.Response>) response;
        mCallback.onSuccessDeliveryCallback(deliveryList);
    }

    @Override
    public void onFailure() {
        mCallback.onFailedDeliveryCallback();
    }

    public void setCallback(MainPresenterCallback callback) {
        mCallback = callback;
    }
}
