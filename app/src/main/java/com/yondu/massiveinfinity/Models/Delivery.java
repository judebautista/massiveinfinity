package com.yondu.massiveinfinity.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Jude Bautista on 18/08/2017.
 */

public class Delivery {



    public class Response implements Serializable {

        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("imageUrl")
        @Expose
        private String imageUrl;
        @SerializedName("location")
        @Expose
        private Location location;
        private final static long serialVersionUID = 4889619025176183885L;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

    }


    public class Location implements Serializable
    {

        @SerializedName("lat")
        @Expose
        private Float lat;
        @SerializedName("lng")
        @Expose
        private Float lng;
        @SerializedName("address")
        @Expose
        private String address;
        private final static long serialVersionUID = 5549673667696660410L;

        public Float getLat() {
            return lat;
        }

        public void setLat(Float lat) {
            this.lat = lat;
        }

        public Float getLng() {
            return lng;
        }

        public void setLng(Float lng) {
            this.lng = lng;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

    }
}
