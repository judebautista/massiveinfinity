package com.yondu.massiveinfinity.Mangers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Remson Cruz on 26/06/2017.
 */

public class DialogManager {

    Context mContext;
    public DialogManager(Context context) {
    mContext = context;
    }

    public void showDialog(String title,String Message)
    {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
