package com.yondu.massiveinfinity.Mangers;

import com.google.gson.Gson;
import com.yondu.massiveinfinity.Interfaces.APIInterface;
import com.yondu.massiveinfinity.Interfaces.APIManagerCallback;
import com.yondu.massiveinfinity.Models.Delivery;


import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jude Bautista on 05/06/2017.
 */
public class APIManager {

    public APIInterface api;
    public Object response;
    public APIManagerCallback mCallback;
    public Gson gson;
    public APIManager() {
        gson = new Gson();
        api = new RetrofitManger().getClientJSON();
    }

    public void getDeliveries() {

        Observable<List<Delivery.Response>> deliveryDetails = api.deliveries();
        deliveryDetails.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<List<Delivery.Response>>() {

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Delivery.Response> value) {
                response = value;
            }

            @Override
            public void onError(Throwable e) {
                mCallback.onFailure(""+e);
            }

            @Override
            public void onComplete() {
                mCallback.onSuccess(response);

            }
        });

    }



    public void setCallback(APIManagerCallback callback) {
        mCallback = callback;
    }


}
